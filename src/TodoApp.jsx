import { useState } from "react";
import { useGetTodoByIdQuery, useGetTodosQuery } from "./store/apis";

export const TodoApp = () => {
    // const { data: todos= [], isLoading } = useGetTodosQuery();

    const [todoId, setTodoId] = useState(1);

    const { data: todo, isLoading } = useGetTodoByIdQuery( todoId );
    console.log( todo );

    const nextTodo = ()=> {
        setTodoId( todoId + 1 );
    };

    const previousTodo = ()=>{
        if( todoId === 1 ) return;
        setTodoId( todoId - 1 );
    };

    return (
        <>
            <h1> TodoApp RTK-QUERY</h1>
            <hr />
            <h4>IsLoading...</h4>
        
            <pre>{ JSON.stringify(todo) }</pre>
            

            <button onClick={ previousTodo }> Previous Todo</button>
            <button onClick={ nextTodo }> Next Todo  </button>


            {/* <ul>
                { 
                    todos.map( todo => (
                        <li key={ todo.id }>
                           <strong> {todo.completed ? 'DONE' : 'Pedning'} </strong>  { todo.title }
                        </li>
                    ))}
            </ul> */}

        
        </>
    );
}
