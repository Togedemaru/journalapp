import { pokemonApi } from "../../../api/pokemonApi";
import { setPokemons,  startLoadingPokemons } from "./pokemonSlice";



export const getPokemons = ( page = 0 ) => {
    return async ( dispatch, getState ) => {
        dispatch( startLoadingPokemons() );

        //TODO: realizar peticion http a la API
        // const resp = await fetch(`pokemon?limit=10&offset=${ page * 10 }`);
        // const data = await resp.json();
        // console.log(data.results);

        const { data } = await pokemonApi.get(`pokemon?limit=10&offset=${ page * 10 }`);
        dispatch( setPokemons( {pokes: data.results, page: page + 1 } ) );
    }
};