import { createSlice } from '@reduxjs/toolkit';

export const pokemonSlice = createSlice({
    name: 'pokemon',
    initialState: {
        page: 0,
        pokes: [],
        isLoading: false
    },
    reducers: {
        startLoadingPokemons: (state, /* action */ ) => {
            state.isLoading = true;
        },
        setPokemons: (state,  action  ) => {
            state.isLoading = false;
            state.page = action.payload.page;
            state.pokes = action.payload.pokes;
        }
    }
});


export const { startLoadingPokemons, setPokemons } = pokemonSlice.actions;